from flask import Flask, render_template, url_for, request, redirect
from save import save_to_file
app = Flask(  # Create a flask app
	__name__,
	template_folder='templates',  # Name of html file folder
	static_folder='static'  # Name of directory for static files
)


@app.route('/')
def index():
    style = url_for('static', filename='style.css')
    return render_template('index.html', style=style)

@app.route('/rfc')
def rfc():
    style = url_for('static', filename='style.css')
    return render_template('rfc.html', style=style)

@app.route('/create', methods=['GET', 'POST'])
def create():
    style = url_for('static', filename='style.css')
    if request.method == 'POST':
        title = request.form['title']
        author = request.form['author']
        references = request.form['references']
        body = request.form['body']
        if not title or not author or not references or not body:
            return render_template('create.html', style=style, error='Please fill out all fields')  
        else:
            save_to_file(title, author, references, body)
            return redirect(url_for('submitted')) # TODO: Add to database and redirect to submitted page

    return render_template('create.html', style=style, error = "")

@app.route('/submitted')
def submitted():
    style = url_for('static', filename='style.css')
    return render_template('submitted.html', style=style,)

app.run(host='0.0.0.0', port=81)
